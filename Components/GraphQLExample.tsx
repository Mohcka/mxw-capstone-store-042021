import { useMutation, useQuery } from "@apollo/client";
import {
  TableContainer,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Container,
  Box,
  TextField,
  Button,
} from "@material-ui/core";
import React, { ReactEventHandler, useEffect, useState } from "react";
import {
  ADD_PRODUCT,
  CreateProductVars,
  GET_PRODUCTS,
} from "../graphql/schemas/products.schema";
import { ADD_PROJECT } from "../graphql/schemas/projects";
import { Product } from "../src/models/product";

export default function GraphQLExample() {
  let input: HTMLInputElement;
  let quantityInput: HTMLInputElement;
  let priceInput: HTMLInputElement;
  const [products, setProducts] = useState<Product[]>([]);
  const ID = 2;

  type createProductMutation = {
    createProduct: Product;
  };

  const { loading, error, data, refetch } = useQuery<{ products: Product[] }>(
    GET_PRODUCTS
  );
  const [addProduct, { data: upData, error: createProjectQueryErr }] = useMutation<
    createProductMutation,
    CreateProductVars
  >(ADD_PRODUCT);

  useEffect(() => {
    // if the GET_PRODUCTS query is done loading, populate products state
    if (!loading) {
      setProducts(data.products);
    }
    return () => {};
  }, [data]);

  useEffect(() => {
    if (createProjectQueryErr) console.log({...createProjectQueryErr});

    console.log('hello');
    

    return () => {};
  }, [createProjectQueryErr]);

  const updatenameValue = (e: React.ChangeEvent<HTMLInputElement>) => {};

  /**
   * Call the api to create an ew product
   */
  const createProduct = () => {
    addProduct({
      variables: {
        name: input.value,
        quantity: parseInt(priceInput.value),
        price: parseFloat(priceInput.value),
        departmentId: "0763a35a-b8a7-4421-8ee1-e96d0f7852eb",
      },
    }).then((d) => {
      console.log(d);
      console.log('REFETCHING...');
      
      refetch();
    });
    input.value = "";
    quantityInput.value = "";
    priceInput.value = "";
  };

  return (
    <Container>
      <Box my={5}>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Product</TableCell>
                <TableCell align="right">Price</TableCell>
                <TableCell align="right">Qunatity</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {!loading
                ? products.map((row) => (
                    <TableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell align="right">{row.price}</TableCell>
                      <TableCell align="right">{row.quantity}</TableCell>
                    </TableRow>
                  ))
                : "Loading"}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <Box>
        <Box>
          <TextField
            inputRef={(node: HTMLInputElement) => (input = node)}
            required
            id="product-name-input"
            label="Product Name"
          />
          <TextField
            inputRef={(node: HTMLInputElement) => (quantityInput = node)}
            required
            id="product-quantity-input"
            label="Product Quantity"
          />
          <TextField
            inputRef={(node: HTMLInputElement) => (priceInput = node)}
            required
            id="product-price-input"
            label="Product Price"
          />
          <Button variant={"contained"} onClick={createProduct}>
            SUBMIT
          </Button>
        </Box>
      </Box>
    </Container>
  );
}
