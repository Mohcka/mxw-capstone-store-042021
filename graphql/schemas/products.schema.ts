import { gql } from "@apollo/client";

export const GET_PRODUCTS = gql`
  query Products {
    products {
      id
      name
      quantity
      price
      department {
        name
      }
    }
  }
`;

export type CreateProductVars = {
  name: string
  quantity: number
  price: number
  departmentId: string
}

export const ADD_PRODUCT = gql`
  mutation CreateProduct($departmentId: Uuid!, $name: String, $quantity: Int!, $price: Decimal!) {
    createProduct(departmentId: $departmentId, name: $name, quantity: $quantity, price: $price) { 
      id
      name
      price
      quantity
      department{
        name
      }
    }
  }
`;
