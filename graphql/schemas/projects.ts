import gql from "graphql-tag";

export const GET_PROJECTS = gql`
  query Projects {
    projects {
      id
      name
      createdBy
    }
  }
`

export const ADD_PROJECT = gql`
  mutation CreatProject($name: String!, $createdBy: String!) {
    createProject(name: $name, createdBy: $createdBy) {
      id
      name
      createdBy
    }
  }
`
