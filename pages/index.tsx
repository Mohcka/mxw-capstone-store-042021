import Head from "next/head";
import { useEffect, useState } from "react";
import {
  ApolloClient,
  ApolloProvider,
  gql,
  InMemoryCache,
  NormalizedCacheObject,
  useQuery,
} from "@apollo/client";
import styles from "../styles/Home.module.css";
import GraphQLExample from "../Components/GraphQLExample";

export default function Home() {
  const client = new ApolloClient({
    uri: "https://localhost:5001/graphql",
    cache: new InMemoryCache(),
  });
  
  return (
    <ApolloProvider {...{ client }}>
      <GraphQLExample />
    </ApolloProvider>
  );
}
