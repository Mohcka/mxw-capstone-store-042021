import { Product } from "./product";

export interface Department {
  id: string;
  name: string;
  products: Product[];
}