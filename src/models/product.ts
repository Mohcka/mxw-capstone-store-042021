import { Department } from "./department";

export interface Product {
  id: string;
  name: string;
  quantity: number;
  price: number;
  department: Department;
}